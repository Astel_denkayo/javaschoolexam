package com.tsystems.javaschool.tasks.calculator;

import java.text.NumberFormat;

public class Calculator {

    private class Result
    {

        public double acc;
        public String rest;

        public Result(double v, String r) {
            this.acc = v;
            this.rest = r;
        }
    }

    private Result PlusMinus(String statement)
    {
        Result current = MulDiv(statement);
        if (current == null) return null;

        double acc = current.acc;

        while (current.rest.length() > 0) {
            if (!(current.rest.charAt(0) == '+' || current.rest.charAt(0) == '-')) break;

            char sign = current.rest.charAt(0);
            String next = current.rest.substring(1);

            acc = current.acc;

            current = MulDiv(next);
            if (current == null) return null;

            if (sign == '+') {
                acc += current.acc;
            } else {
                acc -= current.acc;
            }
            current.acc = acc;
        }
        return new Result(current.acc, current.rest);
    }

    private Result MulDiv(String statement)
    {
        Result current = Bracket(statement);
        if (current == null) return null;

        double acc = current.acc;
        while (true) {
            if (current.rest.length() == 0) {
                return current;
            }
            char sign = current.rest.charAt(0);
            if ((sign != '*' && sign != '/')) return current;

            String next = current.rest.substring(1);
            Result right = Bracket(next);
            if (right == null) return null;

            if (sign == '*') {
                acc *= right.acc;
            } else {
                if (right.acc == 0) return null;
                acc /= right.acc;
            }

            current = new Result(acc, right.rest);
        }
    }

    private Result Bracket(String statement)
    {
        char zeroChar = statement.charAt(0);
        if (zeroChar == '(') {
            Result r = PlusMinus(statement.substring(1));
            if (r == null) return null;
            if (!r.rest.isEmpty() && r.rest.charAt(0) == ')') {
                r.rest = r.rest.substring(1);
            } else {
                return null;
            }
            return r;
        }
        return Num(statement);
    }


    private Result Num(String statement) {
        int i = 0;
        int dot_cnt = 0;
        // digits and dot
        while (i < statement.length() && (Character.isDigit(statement.charAt(i)) || statement.charAt(i) == '.')) {
            if (statement.charAt(i) == '.' && ++dot_cnt > 1) {
                return null; //not one dot
            }
            i++;
        }
        if (i == 0) { // not valid num
            return null;
        }

        double dPart = Double.parseDouble(statement.substring(0, i));

        String restPart = statement.substring(i);

        return new Result(dPart, restPart);
    }

    private String resultFormat(double result)
    {
        NumberFormat formatter = NumberFormat.getNumberInstance();
        formatter.setMaximumFractionDigits(4);
        formatter.setMinimumFractionDigits(0);

        return formatter.format(result).replaceFirst(",",".");
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if ((statement == null) || (statement.isEmpty())) return null;
        Result result = PlusMinus(statement);
        if (result == null) return null;
        if (!result.rest.isEmpty()) {
            return null;
        }
        return resultFormat(result.acc);
    }

}