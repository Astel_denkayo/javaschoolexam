package com.tsystems.javaschool.tasks.duplicates;

import java.io.File;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class DuplicateFinder {

    public TreeMap<String, Integer> lines;


    DuplicateFinder()
    {
        lines = new TreeMap<String, Integer>();
    }

    /**
     * Processes the specified file and puts into another sorted and unique
     * lines each followed by number of occurrences.
     *
     * @param sourceFile file to be processed
     * @param targetFile output file; append if file exist, create if not.
     * @return <code>false</code> if there were any errors, otherwise
     * <code>true</code>
     */
    public boolean process(File sourceFile, File targetFile) {

        if ((sourceFile == null) || (targetFile == null)) {
            throw new IllegalArgumentException();
        }

        if (!(sourceFile.exists()) || !(sourceFile.canRead())) {
            System.out.print("Can not found " + sourceFile.getAbsolutePath());
            return false;
        }


        if (!targetFile.exists()) {
            try {
                targetFile.createNewFile();
            } catch (Exception e) {
                System.out.print("Can not create " + targetFile.getAbsolutePath());
                return false;
            }

        }

        try
        {

            Scanner s = new Scanner(sourceFile);
            while (s.hasNextLine()) {
                String temp = s.nextLine();
                Integer count = lines.get(temp);
                lines.put(temp, count == null ? 1 : ++count);
            }
        }
        catch (Exception e)
        {
            return false;
        }

        try
        {
            FileWriter writer = new FileWriter(targetFile, true);
            for (Map.Entry entry : lines.entrySet()) {
                writer.write(entry.getKey() + "[" + entry.getValue() + "]\r\n");
            }
            writer.close();
        }
        catch (Exception e)
        {
            return false;
        }

        return true;
    }

}
