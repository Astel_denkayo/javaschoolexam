package com.tsystems.javaschool.tasks.subsequence;

import java.util.Iterator;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if ((x == null) || (y == null))
            throw new IllegalArgumentException();

        Iterator xIter = x.iterator();
        if (!xIter.hasNext()) return true;
        Object xElement = xIter.next();

        for (Iterator yIter = y.iterator(); yIter.hasNext(); ) {
            Object yElement = yIter.next();
            if (yElement == xElement)
            {
                if (!xIter.hasNext()) return true;
                xElement = xIter.next();
            }
        }
        return false;
    }
}
