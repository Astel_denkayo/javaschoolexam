package com.tsystems.javaschool.tasks.duplicates;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
//import org.junit.rules.TemporaryFolder;

//import org.junit.rules.TemporaryFolder;

import javax.annotation.Resource;
import java.io.File;
import java.nio.file.Files;
import java.util.Map;

import static java.nio.file.StandardCopyOption.*;

public class MyDuplicateFinderTest {


    //public DuplicateFinderTest() {}

    private DuplicateFinder duplicateFinder = new DuplicateFinder();
    String path = "src/test/res/";
    String test = "test.txt";


    @Test(expected = IllegalArgumentException.class)
    public void test() {
        //run
        duplicateFinder.process(null, new File("a.txt"));

        //assert : exception
    }

    @Test(expected = IllegalArgumentException.class)
    public void test1() {
        //run
        duplicateFinder.process(new File("a.txt"), null);

        //assert : exception
    }

    //test list content
    @Test
    public void test2() {
        //define
        File source = new File(path + "a1.txt");
        File target = new File(path + test);
        String expectedResult = "aaa[1]\r\n" +
                "bbb[1]\r\n" +
                "ccc[1]\r\n" +
                "ddd[3]\r\n";


        //run
        duplicateFinder.process(source, target);

        String result = "";
        for (Map.Entry entry : duplicateFinder.lines.entrySet()) {
            result = new String( result + (entry.getKey() + "[" + entry.getValue() + "]\r\n"));
        }
        //assert
        Assert.assertEquals(expectedResult,result);

    }

    //empty a2.txt
    @Test
    public void test3() {
        //define
        File source = new File(path + "a2.txt");
        File target = new File(path + "b2.txt");
        //run
        boolean result = duplicateFinder.process(source, target);
        //assert
        Assert.assertTrue(result);
    }

    //file a3.txt not exists
    @Test
    public void test4() {
        //define
        File source = new File(path + "a3.txt");
        File target = new File(path + "b3.txt");
        //run
        boolean result = duplicateFinder.process(source, target);
        //assert
        Assert.assertFalse(result);
    }

    //create target
    @Test
    public void test5() {
        //define
        File source = new File(path + "a4.txt");
        File target = new File(path + "b4.txt");
        //run
        if (target.exists()) target.delete();
        boolean result = duplicateFinder.process(source, target);

        //assert
        Assert.assertTrue(target.exists());
    }


    //append file
    @Test
    public void test6() {
        //define
        File source = new File(path + "a5.txt");
        File target = new File(path + "test.txt");
        long expectedResult = 67;

        if (target.exists()) target.delete();
        try
        {
            Files.copy(new File(path + "b5.txt").toPath(), new File(path + "test.txt").toPath(),REPLACE_EXISTING);
        }
        catch(Exception e)
        {
            Assert.fail();
        }

        //run
        duplicateFinder.process(source, target);
        long result = 0;
        try {
            result = Files.size(target.toPath());
        }
        catch(Exception e)
        {
            Assert.fail();
        }
        //assert
        Assert.assertEquals(expectedResult, result);
    }
}